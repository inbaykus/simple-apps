var jwtStrategy = require('passport-jwt').Strategy;
// ExtractJwt = require('passport-jwt').ExtractJwt;
var knex =require('../db/knex');
var config = require('../knexfile');

module.exports = function (passport) {
	var opts = {};
	// opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
	opts.secretOrKey = config.secret;
	passport.use(new jwtStrategy(opts, function (jwt_payload, done) {
		var id = jwt_payload.id;
		knex('users')
			.where({id})
			.first()
			.then(function (err, user) {
				if (err) {
					return done(err, false);
				}
				if (user) {
					done(null, user);
				} else {
					done(null, false);
				}
			});
	}));
};