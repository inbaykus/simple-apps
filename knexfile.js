module.exports = {
	// development:{
	// 	client : 'mysql',
	// 	connection : process.env.DATABASE_URL || 'postgres://postgres:database@localhost:5432/simpleapp' 
	// },
	// production:{
	// 	client : 'postgresql',
	// 	connection : process.env.DATABASE_URL || 'postgres://postgres:database@localhost:5432/simpleapp' 
	// },
	development:{
		client : 'mysql',
		connection : {
			host : '127.0.0.1',
			user : 'root',
			passowrd: '',
			database: 'simpleapp',
			charset: 'utf8'
		}
	},
	secret: 'secretKey'
}