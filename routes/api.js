var express = require('express');
var passport = require('passport');
var knex = require('../db/knex');
var bcrypt = require('bcryptjs');
var jwt = require('jwt-simple');
var config = require('../knexfile');

require('../config/passport')(passport);

var apiRoute = express.Router();
getToken = function (headers) {
	if (headers && headers.Authorization) {
		var parted = headers.Authorization.split(' ');
		if (parted.length === 2) {
			console.log('parted successful!');
			return parted[1];
		} else {
			return null;
		}
	} else {
		return null;
	}
};

apiRoute.post('/signup', function (req, res) {
	if (!req.body.username || !req.body.password){
		res.json({success: false, msg: 'Username and password required!'});
	} else {
		var passwordHash = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
		var user = {
			name: req.body.name,
			username: req.body.username,
			password: passwordHash
		};
		knex('users')
			.insert(user)
			.then(function (){
					res.json({success: true, msg: 'You are registered!'});
					// req.flash('success_msg', 'You are registered!')
					// res.redirect('/users/login');
			});
	}
});

apiRoute.post('/authenticate', function (req, res) {
	var username = req.body.username;
	var password = req.body.password;
	knex('users')
		.where({username})
		.first()
		.then(function (user) {
			if (!user) {
				return res.json({success: false, msg: 'User not found!'});
			}
			if (!bcrypt.compareSync(password, user.password)){
				return res.json({success: false, msg: 'Password not matched!'});
			} else {
				var token = jwt.encode(user, config.secret);
				res.json({success: true, token: 'JWT '+token});
			}
		});
});

// apiRoute.get("/secret",passport.authenticate('jwt', { session: false }), function(req, res){
//   res.json({message: "Success! You can not see this without a token"});
// });
apiRoute.get('/memberinfo', passport.authenticate('jwt', {session: false}), function (req, res){
	return res.send(req.headers.Authorization);
	// var token = getToken(req.headers);
	// console.log(token);
	// if (token) {
	// 	res.json({msg: 'You access private page'});
	// 	var decoded = jwt.decode(token, config.secret);
	// 	var name = decoded.name;
	// 	knex('users')
	// 		.where({name})
	// 		.first()
	// 		.then(function (user){
	// 			return res.json({success: true, msg: 'Welcome mas '+ user.name + '!'});
	// 		});
	// } else {
	// 	return res.json({success: false, msg: 'No token provided!'});
	// }
});


module.exports = apiRoute;