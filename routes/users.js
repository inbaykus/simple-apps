var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
const knex = require('../db/knex');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
//login
router.get('/login', function (req, res) {
	res.render('login');
});

router.get('/register', function (req, res) {
	res.render('register');
});

router.get('/dashboard', function (req, res) {
	res.render('dashboard');
});

router.post('/register', function (req, res) {
	var name = req.body.name;
	var email = req.body.email;
	var username = req.body.username;
	var password = req.body.password;
	var password2 = req.body.password2;

	req.checkBody('name','Name is required').notEmpty();
	// req.checkBody('email', 'Email is required').notEmpty();
	// req.checkBody('email', 'Email is not valid').isEmail();
	req.checkBody('username', 'Username is required').notEmpty();
	req.checkBody('password', 'Password is required').notEmpty();
	req.checkBody('password2', 'Password confirmation is required').notEmpty();
	req.checkBody('password2', 'Password does not match').equals(req.body.password);

	var errors = req.validationErrors();
	if (errors) {
		res.render('register',{
			errors: errors
		});
	} else {
		console.log('not error');
		var passwordHash = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
		var user = {
			name: req.body.name,
			username: req.body.username,
			password: passwordHash
		};
		knex('users')
			.insert(user)
			.then(function (){
				req.flash('success_msg', 'You are registered!')
				res.redirect('/users/login');
			});
		
	}
});

passport.use(new LocalStrategy(
  function(username, password, done) {
    knex('users')
    	.where({username})
    	.first()
    	.then(function (user) {
    		if (!user) return done(null, false);

    		if (!bcrypt.compareSync(password, user.password)) {
    			return done(null,false);
    		} else {
    			return done(null,user);
    		}
    	})
    	.catch(function (err) {
    		return done(err);
    	});
	}
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  knex('users')
  	.where({id})
  	.first()
  	.then(function (user) {
  		done(null, user);
  	})
  	.catch(function (err) {
  		done(err, null);
  	});
});

router.post('/login', 
	passport.authenticate ('local', { 
		successRedirect: '/users/dashboard',
  		failureRedirect: '/users/login'}),
	function (req, res) {
		res.redirect('/users/dashboard/' +req.user.username)
	}
);

module.exports = router;