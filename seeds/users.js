
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      var users = [{
        name: 'Indra Bayu',
        username: 'Inbay',
        password: 'Indra94'
      }, {
        name: 'Arifin Budi',
        username: 'prasIpin',
        password: 'ArifinKecu'
      }, {
        name: 'Adi Rayhan',
        username: 'Adiiw',
        password: 'adiLaw'
      }]
      // Inserts seed entries
      return knex('users').insert(users);
    });
};
