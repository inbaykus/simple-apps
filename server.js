var express = require('express');
var expresshandle = require('express-handlebars');
var bodyParser = require('body-parser');
var expressValidate = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var path = require('path');
var PORT = process.env.PORT || 3000;
// var db = require('./databasejs');

var routes = require('./routes/index');
var users = require('./routes/users');
var apiRoute = require('./routes/api');

// inisialisasi server
var app = express();

// untuk view dan menampilkan layout html dengan 
// handlebars
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', expresshandle({extname:'handlebars',defaultLayout: 'layout'}));
app.set('view engine', 'handlebars');

// passport
app.use(passport.initialize());
app.use(passport.session());

// bodyParser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// validator
app.use(expressValidate({
	errorFormatter: function (param, msg, val) {
		var namespace = param.split('.')
		, root = namespace.shift()
		, formParam = root;

		while(namespace.length) {
			formParam += '[' + namespace.shift() + ']';
		}
		return {
			param: formParam,
			msg: msg,
			val: val
		};
	}
}));

//static folder, guna static buat biar bisa static lol
app.use(express.static(path.join(__dirname,'public')));

//session
app.use(session({
	secret: 'secret',
	saveUnitialized: true,
	resave: true
}));

//flash
app.use(flash());

//global error
app.use(function (req, res, next) {
	res.locals.success_msg = req.flash('success_msg');
	res.locals.error_msg = req.flash('error_msg');
	res.locals.error = req.flash('error');
	res.locals.user = req.user || null;
	next();
});

//routes
app.use('/', routes);
app.use('/users', users);
app.use('/api', apiRoute);

app.listen(PORT, function () {
	console.log('Server running on port: '+PORT);
});